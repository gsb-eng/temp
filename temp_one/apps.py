from __future__ import unicode_literals

from django.apps import AppConfig


class TempOneConfig(AppConfig):
    name = 'temp_one'
