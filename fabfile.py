from fabric.api import local

def prepare_deploy():
    local("./manage.py test")
    local("git add . && git commit -m 'auto deploy'")
    local("git push origin master")
